########################################################################
#
# 	PURPOSE	SCRIPT FOR CREATING NEW DJANGO APP
# 
#	AUTOR	CE
#	DATE	2020-01-28
#
########################################################################

######		T A S K S						S T A T U S	
#			READ PROJECT NAME				DONE
#			CREATE PROJECT FOLDER			DONE
#			CREATE VIRTUAL ENVIRONMENT		IN PROGRESS
#			ACTIVATE VIRTUAL ENVIRONMENT	TODO
#			INSTALL DJANGO					TODO
#			DJANGO-ADMIN STARTPROJECT		TODO
#			PYTHON STARTAPP					TODO
#			PYTHON MIGRATE					TODO
#			INCLUDE APP TO SETTINGS			TODO
#			PYTHON CREATESUPERUSER			TODO
#			PYTHON RUNSERVER

########################################################################
#
#	READ PROJECT NAME, CREATE PROJECT FOLDER
#
########################################################################

###### IMPORT MODULES
import os
import getpass

###### CREATE FUNCTIONS

### DEFINE PRINT FORMAT AND PRINT
def printMessage(value, message):
	#print('{:35}'.format(value) +'{:35}'.format(message))
	print('{:40} \t{:40}'.format(message, value))
	 
### GET THE PROJECTSNAME
def getProjectName():
	projectName = input('Type in project name: ')
	message = 'project name has been typed in' 
	printMessage(projectName, message)
	#print(':50'.format(projectName) +':50'.format(output)
	return projectName

### CATEGORIZE FOLDER TYPE AND CONCATENATE PATH
def setPathOfFolder(isSpecificProjectFolder=False, folderName='Projekte'):
	# PRINT TAKEN PARAMETERS
	printMessage(str(isSpecificProjectFolder), 'isSpecificProjectFolder')
	printMessage(folderName, 'folderName')
	# CASE: PARAMETERS NOT CORRECT
	if not isinstance(isSpecificProjectFolder, bool) and isinstance(folderName, string):
		path = "/home/" +getpass.getuser()
		exit(1)
	# CASE: IS GENERAL PROJECT FOLDER
	elif (isSpecificProjectFolder == True):
		path = "/home/" +getpass.getuser() +"/Projekte/" +folderName 
	# CASE: IS SPECIFIC PROJECT FOLDER
	elif (isSpecificProjectFolder == False):
		path = "/home/" +getpass.getuser() +'/' +folderName
	# CASE: WHEN SOMETHING DIFFERENT WENT TOTALY WRONG
	else:
		exit(-1)
	message = 'project path has been set'
	#print(':50'.format(path) +':50'.format(output)
	printMessage(path, message)
	return path

### CREATE NEW FOLDER
def createNewFolder(newPath):
	message = 'path has been created'
	#print(newPath +'\t has been created')
	printMessage(newPath, message)
	os.makedirs(newPath)

### CHECK EXISTANCE OF FOLDER
def checkExistanceOfFolder(path):
	if os.path.exists(path):
		message = 'path already exists'
		#print(path +'\t already exists')
		printMessage(path, message)
		return True
	else:
		message = 'path does not exist'
		#print(path + '\t does not exist')
		printMessage(path, message)
		return False
		
########################################################################

### HANDLE NEW PROJECT NAME
print('\n### HANDLE NEW PROJECT NAME')
projectName = getProjectName()
			
### HANDLE GENERAL PROJECTS FOLDER
print('\n### HANDLE GENERAL PROJECTS FOLDER')
# SET GENERAL PROJECT PATH
print('# SET GENERAL PROJECT PATH')
generalPath = setPathOfFolder()
# CREATE GENERAL PROJECTS PATH
print('# CREATE GENERAL PROJECTS PATH')
if not checkExistanceOfFolder(generalPath):
	createNewFolder(generalPath)

### HANDLE NEW PROJECT FOLDER
print('\n### HANDLE NEW PROJECT FOLDER')
# SET PROJECT PATH
print('# SET PROJECT PATH')
specificPath = setPathOfFolder(True, projectName) if projectName != '' else setPathOfFolder(True)

# CREATE PROJECT PATH 
print('# CREATE PROJECT PATH')
if not checkExistanceOfFolder(specificPath):
	createNewFolder(specificPath)

########################################################################
#
#	SET IMPORTANT SYSTEM PARAMETERS AND DEFINE COMMAND LINE FUNCTIONS
#
########################################################################

### IMPORT MODULES
import os
import subprocess
import sys

### GET ANSWER FOR YES/NO
def returnInputChoiceYN(message):
	# DEFINE CHOICES
	yes = 	{'yes','y', 'ye', ''}
	no 	=	{'no','n'}	
	# GET CHOICE
	choice = input(message).lower()
	consoleText = 'has been typed in '
	printMessage(choice, consoleText)
	# CHECK CHOICE AND RETURN RESULT 
	if choice in yes:
		printMessage( 'True', 'has been returned')
		return True
	elif choice in no:
		printMessage( 'False', 'has been returned')
		return False
	else:
		printMessage( 'invalid input', 'function has been called again')
		returnInputChoiceYN(message)

### EXECUTE SHELL COMMAND AND GET STATUS CODE	
def executeShellCommandReturnStatus(cmd):
	print('### EXECUTE SHELL COMMAND AND GET STATUS CODE')
	try:
		returnedStatus = subprocess.check_call(cmd, shell=True, stderr=subprocess.STDOUT)
		printMessage(cmd, ('returned status = ' + str(returnedStatus)))
		return returnedStatus
	except subprocess.CalledProcessError as e:
		printMessage(cmd, ('returned status = ' + '1'))
		return 1

### EXECUTE SHELL COMMAND AND GET OUTPUT STRING	
def executeShellCommandReturnOutput(cmd):
	print('### EXECUTE SHELL COMMAND AND GET OUTPUT STRING	')
	try:
		returnedStatus = subprocess.check_call(cmd, shell=True, stderr=subprocess.STDOUT)
		printMessage(cmd, ('returned status = ' + str(returnedStatus)))
		if returnedStatus == 0:
			returnedOutput = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
			printMessage(cmd, ('returned output = ' + str(returnedOutput)))			
			return returnedOutput
		else:
			printMessage(cmd, ('returned output = ' + str(returnedOutput)))
			return "no output"
	except subprocess.CalledProcessError as e:
		#return e.output
		printMessage(cmd, ('returned status = ' + 'e'))
		return e.output
		
#### SET PROMPTS FOR PYTHON AND PIP IN REGARD TO VERSION AND PATH
# SET GENERAL COMMAND PROMPTS
pythonCommand = 'python'
pipCommand = 'pip'
# CHECK CURRENTLY USED VERSION
printMessage('sys.version', sys.version)
# RESET COMMAND PROMPT BY ADDING 3
cmd = 'python3 --version'
if (executeShellCommandReturnStatus(cmd) == 0):
	if (sys.version_info[0] >= 3):
		pythonCommand = 'python3'
		pipCommand = 'pip3'
# OUTPUT THE SETTED COMMAND PROMPTS
msg = 'will be used'
printMessage(pythonCommand, msg)
printMessage(pipCommand, msg)

########################################################################
#
#	CREATE VIRTUAL ENVIRONMENT, ACTIVATE VIRTUAL ENVIRONMENT
#
########################################################################

# DECIDE IF VIRTUAL ENVIRONMENT SHALL BE USED
print('# DECIDE IF VIRTUAL ENVIRONMENT SHALL BE USED')
statusOfVenv = returnInputChoiceYN('Shall virtual environment shall be used? [Y/N]')

# CHECK INSTALLATION
def checkInstallationOfApplication(nameOfApplication, nameOfPackageManager):
	print('# CHECK INSTALLATION')
	#	CHECK PACKAGE MANAGER
	listOfPackageManager = ['apt', 'pip', 'pip3']
	additionalParameter = ''
	if nameOfPackageManager.lower() in listOfPackageManager:
		if  nameOfPackageManager.lower() == 'apt':
			additionalParameter = ' --installed'
	else:
		print(nameOfPackageManager +' unknown. Try apt, pip or pip3 instead')
		return -1
	## SET COMMAND	
	cmd = '{} list{} | grep {} | wc -l'.format(nameOfPackageManager, additionalParameter, nameOfApplication)
	## RUN COMMAND
	try:
		listOfVenvInstallations = executeShellCommandReturnStatus(cmd)
		if listOfVenvInstallations == 0:
			return True
		else:
			return False
	except subprocess.CalledProcessError as e:
		printMessage(cmd, ('returned status = ' + 'e'))
		return e.output

# RUN INSTALLATION PROCEDURES
def runInstallationOfApplication(nameOfApplication, nameOfPackageManager):
	print('# RUN INSTALLATION PROCEDURES')
	#	CHECK PACKAGE MANAGER
	additionalSudoRight = ''
	additionalParameter = ''
	if nameOfPackageManager.lower() in listOfPackageManager:
		if  nameOfPackageManager.lower() == 'apt':
			additionalSudoRight = 'sudo '
	else:
		print(nameOfPackageManager +' unknown. Try apt, pip or pip3 instead')
		return -1
	## SET COMMAND
	cmd = '{}{} install {}'.format(additionalSudoRight, nameOfPackageManager, nameOfApplication)
	## RUN COMMAND
	cmd = 'sudo apt install ' +nameOfApplication
	try:
		runVenvInstallation = executeShellCommandReturnStatus(cmd)
	except subprocess.CalledProcessError as e:
		printMessage(cmd, ('returned status = ' + 'e'))
		return e.output
		

# CHECK INSTALLATION OF VIRTUAL ENVIRONMENT

# INSTALL VIRTUAL ENVIRONMENT IF NOT EXIST
neccessaryApplication = 'python3-venv'
neccessaryPackageManager = 'apt'
checkOfInstallation = checkInstallationOfApplication(neccessaryApplication, neccessaryPackageManager)
runOfInstallation = runInstallationOfApplication(neccessaryApplication, neccessaryPackageManager)
aaa
# CREATE VIRTUAL ENVIRONMENT (IF IT SHALL BE USED)
if statusOfVenv == True:
	print('# CREATE VIRTUAL ENVIRONMENT')	
	# GET NAME OF VIRTUAL ENVIRONMENT
	nameOfVenv = input('Type name of virtual environment ')
	# CREATE PATH, FOLDER AND ENVIRONMENT
	pathOfProj = specificPath
	pathOfVenv = '{}/{}'.format(pathOfProj, nameOfVenv)
	cmd = '{} -m venv {}'.format(pythonCommand, pathOfVenv) 
	#cmd = f'{pythonCommand} -m venv {pathOfVenv}' 
	print(executeShellCommandReturnStatus(cmd))

# ACTIVATE VIRTUAL ENVIRONMENT 
if statusOfVenv == True:
	print('# ACTIVATE VIRTUAL ENVIRONMENT')
	cmd = 'source {} /bin/activate'.format(pathOfVenv)
	print(executeShellCommandReturnStatus(cmd))




